#
# Cookbook Name:: django
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "pip"

directory '/vagrant/project_folder' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

execute 'django_install' do  
   command "sudo pip install django"
end
